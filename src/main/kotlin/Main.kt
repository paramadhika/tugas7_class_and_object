class Film(_namaFilm: String, _popularity: Double = 0.0, _rating: String = "G"){
    val namaFilm = _namaFilm            //properti nama film
    val populer = _popularity           //properti tingkat kepopuleran film
    var ratingFilm = _rating            //properti tingkat keamanan untuk ditonton oleh semua umur

        //get() digunakan untuk mendapatkan value dari rating film menjadi angka
        //yang nantinya digunakan dalam function klasifikasifilm() untuk mengklasifikasikan
        //rating film itu sendiri
        get(){
            return when(field){         //pengklasifikasian berdasarkan rating film dari Amerika Serikat yang terdiri dari
                "G" -> "1"              //Aman dintonton semua umur
                "PG" -> "2"             //Aman ditonton atau bisa didampingi oleh orang tua
                "PG-13" -> "3"          //Umur 13 tahun keatas dan perlu didampingi oleh orang tua
                "R" -> "4"              //Tidak cocok untuk ditonton oleh anak-anak
                "NC-17" -> "5"          //Umur 17 tahun dan kebawah dilarang untuk menonton film ini
                else -> "0"
            }
        }

    //Function untuk mencetak detail informasi atau objek film yang telah dibuat
    fun infoFilm() : String{
        val detailfilm = """
            
            ============== Detail Film ==============
            Nama Film       : $namaFilm
            Popularity      : $populer
            Rating Film     : $ratingFilm
        """.trimIndent()
        return detailfilm
    }
}

fun main() {
    //Membuat objek sample film dengan nilai-nilai yang berbeda dan menyimpannya ke dalam list
    val sampleFilm = listOf(Film("Avengers : Endgame", 8.4, "PG-13"),
                            Film("Joker", 8.4, "R"),
                            Film("Wish Dragon", 7.2, "PG"),
                            Film("Boruto", 7.0, "PG"),
                            Film("Stand By Me Doraemon 2", 8.0)
    )

    //perulangan untuk mengakses list samplefilm yang menyimpan objek-objek film
    for(i in sampleFilm){
        println(i.infoFilm())
        if(i.populer in 8.0 .. 10.0) println(">>>>>>>>Recommended<<<<<<<<<")
        //proses untuk mengklasifikasikan film setelah mendapatkan value dari get() pada objek kelas film
        //dan mengkonversinya ke tipe data integer
        println(klasifikasiFilm(i.ratingFilm.toInt()))
    }
}

//function yang digunakan untuk mengklasifikasikan film dengan menerima parameter rating film dari objek saat dipanggil
fun klasifikasiFilm(rated : Int) : String{
    return when(rated){
        in 0..2 -> "*Aman Untuk Ditonton Semua Umur*"
        in 3..5 -> "!!!Perhatian : Film Mengandung Unsur Kekerasan maupun Konten Dewasa!!!"
        else -> "*Tidak Ada yang perlu Diperhatikan*"
    }
}
